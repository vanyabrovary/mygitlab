# MyGitLab

Simple CI implementation using webhooks.

## INSTALLING

Install dependencies using **apt**

`apt-get install libmojolicious-perl git redis-server`

Install some dependencies from **cpan**

`cpan install Redis::hiredis Time::Piece` 

Get **source code**

`cd /var/www && git clone git@gitlab.com:vanyabrovary/mygitlab.git`


## USING

Command for **start** web server on http://127.0.0.1:8080:

`/var/www/mygitlab/init/start`

Command for **stop** web server:

`/var/www/mygitlab/init/stop`

Command get **status** of web server:

`/var/www/mygitlab/init/status`

For **rebuild** manually:

`/var/www/mygitlab/mygitlab ng_build_allatrack`

`/var/www/mygitlab/mygitlab softdata_to_alla`

Get all available **commands**:

`/var/www/mygitlab/mygitlab cmds`

URIs for **webhook**:

`http://ip:8080/job/ng_build_allatrack`

`http://ip:8080/job/softdata_to_alla`

See also:

`https://docs.mojolicious.org/`

`https://docs.mojolicious.org/Mojolicious/Commands`

Free and Open Source.