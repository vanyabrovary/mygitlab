package App;
use Mojo::Base 'Mojolicious';
use Redis::hiredis;

# Run once at start
sub startup {
  my $self = shift;

  # Redis
  $self->app->{redis} = Redis::hiredis->new();
  $self->app->{redis}->connect('127.0.0.1', 6379);

  # Router
  my $r = $self->routes;

  # Command namespace
  push @{ $self->app->commands->namespaces }, 'Command';

  # Route to controller  
  $r->any('/')->to(controller => 'Job', action => 'index');  
  $r->any('/job')->to(controller => 'Job', action => 'index');
  $r->any('/job/:cmd')->to(controller => 'Job', action => 'cmd');

}

1;

# my $conf = $self->plugin('Config'); $self->secrets($conf->{secrets});
