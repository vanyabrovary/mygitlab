package Command::softdata_to_alla;

use JSON;
use Mojo::Base 'Mojolicious::Command';
use JSON::Parse 'json_file_to_perl';
use File::Spec::Functions;
use Time::Piece;

has usage       => 'USAGE: softdata_to_alla';
has description => 'get fresh code from gitlab.com:allatrack-web/softdata-to-alla.git and buid to web dir';

#---------------------------------------------------------------------------------------

my $git_repo       = 'git@gitlab.com:allatrack-web/softdata-to-alla.git';
my $git_branch     = 'cabinet';

my $path_to        = catdir('var','www','cabinet.allatrack.com');
my $path_from      = catdir('var','src','softdata-to-alla');              # /var/src/softdata-to-alla

my $from_lib       = catdir($path_from,   'lib');                         # /var/src/softdata-to-alla/lib
my $from_node      = catdir($path_from,   'node_modules');                # /var/src/softdata-to-alla/node_modules
my $from_bower     = catdir($path_from,   'bower_components');            # /var/src/softdata-to-alla/bower_components
my $from_uploads   = catdir($path_from,   'uploads');                     # /var/src/softdata-to-alla/uploads
my $from_vendor    = catdir($path_from,   'vendor');                      # /var/src/softdata-to-alla/vendor
my $from_php_cfg   = catfile($path_from,  'db_params.php');               # /var/src/softdata-to-alla/db_params.php

my $from_front     = catdir($from_lib,    'front');                       # /var/src/softdata-to-alla/lib/front/

my $to_node        = catdir($from_front,  'node_modules');                # /var/src/softdata-to-alla/lib/front/node_modules
my $to_bower       = catdir($from_front,  'src', 'bower_components');     # /var/src/softdata-to-alla/lib/front/src/bower_components
my $to_vendor      = catdir($from_lib,    'vendor');                      # /var/src/softdata-to-alla/lib/vendor
my $to_uploads     = catdir($from_lib,    'uploads');                     # /var/src/softdata-to-alla/lib/uploads
my $to_php_cfg     = catfile($from_lib,   'config/db_params.php');        # /var/src/softdata-to-alla/lib/config/db_params.php
my $package_json   = catfile($from_front, 'package.json');                # $package.json

#---------------------------------------------------------------------------------------

sub run {
    say "* BUILDING *";
    
    recreate_path("/$from_lib");

    cmd("git clone -b $git_branch $git_repo /$from_lib");

    cmd("ln -s /$from_node /$to_node");
    cmd("ln -s /$from_bower /$to_bower");
    cmd("ln -s /$from_vendor /$to_vendor");
    cmd("ln -s /$from_uploads /$to_uploads");
    cmd("cp -r /$from_php_cfg /$to_php_cfg");
    
    fix_package_json();
    
    cmd("npm run build --prefix /$from_front") if -d "/$from_front";
    
    recreate_path("/$path_to")                 if -d "/$from_front"; 
    
    cmd("cp -r /$from_lib/* /$path_to")        if -d "/$from_front";
    
    dateup("/$path_to/main-page.html");
    
    say "* DONE *";
}

sub cmd {
    my $cmd = shift; die("Want $cmd!") unless $cmd;
    say " * $cmd";
    say `$cmd`;
}

sub recreate_path {
    my $path = shift; 
    
    die("Want $path!") unless $path;
    
    cmd("rm -rf /$path") if $path ne '/';

    cmd("mkdir -p $path");
    
    die("cant find $path")  
        unless -d $path;
}

sub fix_package_json {
    my $p = json_file_to_perl("/$package_json");
    
    delete $p->{scripts}->{start}                 
        if exists $p->{scripts}->{start};
    
    $p->{scripts}->{build} = 'webpack --progress' 
        if exists $p->{scripts}->{build};
    
    open my $fh, ">", "/$package_json";  
    print $fh encode_json($p);  
    close $fh;
}

sub dateup { 
    `echo '<!-- Build ' \$(date --rfc-3339=ns) ' -->' >> $_[0]` if -e $_[0]; 
}

1;
